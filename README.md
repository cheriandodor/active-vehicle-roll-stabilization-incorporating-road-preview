# Development of a Controller for Active Vehicle Roll Stabilization Incorporating Road Preview

## This project was executed as a part of academic class project for 'Safety and Stability' course instructed by [Dr. Beshah Awalew](https://cecas.clemson.edu/ayalew/)

### The team that executed this project are [Pushpak Ravitej](https://pushpakravitej.com), [Christopher Flegel](https://www.linkedin.com/in/christopher-flegel-4b180ab4/) and [Zhijun Han](https://www.linkedin.com/in/zhijun-h-62347391/)

Currently, this project only includes final paper and the presentation. Rest of the support files will be added shortly. 